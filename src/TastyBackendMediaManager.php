<?php

namespace Drupal\tasty_backend_media;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\system\SystemManager;
use Drupal\user\Entity\Role;
use Drupal\media\Entity\MediaType;
use Drupal\views\Views;

/**
 * Tasty Backend Media Manager service.
 */
class TastyBackendMediaManager {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\system\SystemManager definition.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * Constructs a new TastyBackendMediaManager object.
   */
  public function __construct(MessengerInterface $messenger, SystemManager $system_manager, EntityTypeManagerInterface $entity_type_manager, MenuActiveTrailInterface $menu_active_trail) {
    $this->messenger = $messenger;
    $this->systemManager = $system_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * Add default permissions for a media type.
   *
   * @param Drupal\media\Entity\MediaType $type
   *   Drupal MediaType object.
   * @param string $rid
   *   The ID of a user role to add permissions.
   */
  public function addMediaTypePermissions(MediaType $type, $rid = 'tb_media_admin') {
    $role = Role::load($rid);
    $permissions = [
      'create ' . $type->id() . ' media',
      'delete any ' . $type->id() . ' media',
      'edit any ' . $type->id() . ' media',
    ];
    foreach ($permissions as $permission) {
      $role->grantPermission($permission);
    }
    $role->save();
    $args = [
      '%role_name' => $role->label(),
      '%type' => $type->label(),
    ];
    $this->messenger->addStatus(t('Default media permissions have been added to the %role_name role for %type media.', $args));
  }

  /**
   * Add a new administration view for a media type.
   *
   * @param Drupal\media\Entity\MediaType $type
   *   Drupal MediaType object.
   */
  public function addAdminView(MediaType $type) {

    // Default view doesn't have any type set.
    $type_filter = [
      'id' => 'bundle',
      'table' => 'media_field_data',
      'field' => 'bundle',
      'value' => [
        $type->id() => $type->id(),
      ],
      'entity_type' => 'media',
      'entity_field' => 'bundle',
      'plugin_id' => 'bundle',
      'group' => 1,
    ];

    // Duplicate the view.
    $view = Views::getView('tb_media_manage_media')->storage->createDuplicate();

    // Set some basic info.
    $view->setStatus(TRUE);
    $view->set('id', 'tb_media_manage_media_' . $type->id());
    $view->set('label', 'Tasty Backend Media Manage ' . $type->label());
    $view->set('description', 'Tasty Backend administration view to manage all ' . $type->label() . ' media.');

    // Set the display options.
    $display = $view->get('display');
    $display['default']['display_options']['access']['options']['perm'] = 'edit any ' . $type->id() . ' media';
    $display['default']['display_options']['filters']['bundle'] = $type_filter;
    $display['default']['display_options']['title'] = 'Manage ' . $type->label() . ' media';
    $display['page_1']['display_options']['path'] = 'admin/manage/media/media/' . $type->id();
    $display['page_1']['display_options']['menu']['title'] = $type->label();
    $display['page_1']['display_options']['menu']['description'] = 'Manage ' . $type->label() . ' media.';
    $view->set('display', $display);

    // Save the new view.
    $view->save();
  }

  /**
   * Deletes an administration view for a media type.
   *
   * $media_type
   *    Machine name of media type.
   */
  public function deleteAdminView($media_type) {
    $storage_handler = $this->entityTypeManager->getStorage('view');
    $entities = $storage_handler->loadMultiple(['tb_media_manage_media_' . $media_type]);
    $storage_handler->delete($entities);
  }

}
